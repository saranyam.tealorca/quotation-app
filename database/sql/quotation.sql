-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 04, 2022 at 07:15 PM
-- Server version: 8.0.30-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quotation`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_08_03_124929_create_quotation_table', 2),
(6, '2022_08_04_072509_create_summary_table', 3),
(7, '2022_08_03_094910_create_printers_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

CREATE TABLE `printers` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` bigint UNSIGNED NOT NULL,
  `page_xs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_s` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_m` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_l` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_xl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optimization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `framework` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` longtext COLLATE utf8mb4_unicode_ci,
  `compatibility` longtext COLLATE utf8mb4_unicode_ci,
  `interactive_options` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advanced_js` longtext COLLATE utf8mb4_unicode_ci,
  `additional_options` longtext COLLATE utf8mb4_unicode_ci,
  `page_total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options_total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extraParam` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `page_xs`, `page_s`, `page_m`, `page_l`, `page_xl`, `optimization`, `responsive`, `framework`, `layout`, `compatibility`, `interactive_options`, `advanced_js`, `additional_options`, `page_total`, `options_total`, `total`, `eta`, `extraParam`, `created_at`, `updated_at`) VALUES
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:10:27', '2022-08-04 01:10:27'),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:13:16', '2022-08-04 01:13:16'),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:13:20', '2022-08-04 01:13:20'),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:21:03', '2022-08-04 01:21:03'),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:21:54', '2022-08-04 01:21:54'),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}}', '2022-08-04 01:25:41', '2022-08-04 01:25:41'),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1},\"projectbrief\":{\"desc\":\"This is the sample project for the React team\"},\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 02:08:26', '2022-08-04 02:08:26'),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15},\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":{\"option\":\"advanced css3 animation\",\"costPerHr\":45},\"advanced_js_option\":{\"option\":\"React\",\"costPerHr\":45},\"interactivity_option\":{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1},\"compatibility_option\":{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1},\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 02:09:38', '2022-08-04 02:09:38');

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` bigint NOT NULL,
  `pagesize` longtext COLLATE utf8mb4_unicode_ci,
  `optimize` longtext COLLATE utf8mb4_unicode_ci,
  `responseive` longtext COLLATE utf8mb4_unicode_ci,
  `framework` longtext COLLATE utf8mb4_unicode_ci,
  `layout` longtext COLLATE utf8mb4_unicode_ci,
  `additional_css_option` longtext COLLATE utf8mb4_unicode_ci,
  `advanced_js_option` longtext COLLATE utf8mb4_unicode_ci,
  `interactivity_option` longtext COLLATE utf8mb4_unicode_ci,
  `compatibility_option` longtext COLLATE utf8mb4_unicode_ci,
  `totalcost` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `projectbrief` longtext COLLATE utf8mb4_unicode_ci,
  `attachment` longtext COLLATE utf8mb4_unicode_ci,
  `extraParam` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `name`, `email`, `mobile`, `pagesize`, `optimize`, `responseive`, `framework`, `layout`, `additional_css_option`, `advanced_js_option`, `interactivity_option`, `compatibility_option`, `totalcost`, `projectbrief`, `attachment`, `extraParam`, `created_at`, `updated_at`) VALUES
(8, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:48', '2022-08-04 05:20:48'),
(9, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:49', '2022-08-04 05:20:49'),
(10, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:50', '2022-08-04 05:20:50'),
(11, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:50', '2022-08-04 05:20:50'),
(12, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:50', '2022-08-04 05:20:50'),
(13, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:50', '2022-08-04 05:20:50'),
(14, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:51', '2022-08-04 05:20:51'),
(15, 'Surya', 'surya@tealorca.in', 2342398128, '[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}]', '[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}]', '{\"option\":\"I have One Resolution\",\"pc\":10}', '{\"option\":\"Bootstrap\",\"pc\":10}', '[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}]', '[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}]', '[{\"option\":\"React\",\"costPerHr\":45}]', '[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}]', '[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}]', '3534', '\"This is the sample project for the React team\"', 'ugjkhgkjh', '{\"totalcost\":3534,\"pagesize\":[{\"option\":\"S\",\"price\":2342,\"eta\":3,\"qty\":1},{\"option\":\"M\",\"price\":5000,\"eta\":3,\"qty\":3}],\"optimize\":[{\"option\":\"Section 508 \\/ WCAG\",\"pc\":15}],\"responseive\":{\"option\":\"I have One Resolution\",\"pc\":10},\"framework\":{\"option\":\"Bootstrap\",\"pc\":10},\"layout\":[{\"option\":\"Retina\",\"pc\":0},{\"option\":\"Google fonts\",\"pc\":0}],\"additional_css_option\":[{\"option\":\"advanced css3 animation\",\"costPerHr\":45}],\"advanced_js_option\":[{\"option\":\"React\",\"costPerHr\":45}],\"interactivity_option\":[{\"option\":\"Standard interactivity_option\",\"cost\":43,\"pc\":12,\"qty\":1}],\"compatibility_option\":[{\"option\":\"Another\\/older browser\",\"pc\":12,\"qty\":1}],\"projectbrief\":\"This is the sample project for the React team\",\"attachment\":\"ugjkhgkjh\",\"contact\":{\"name\":\"Surya\",\"email\":\"surya@tealorca.in\",\"mobile\":\"2342398128\"}}', '2022-08-04 05:20:51', '2022-08-04 05:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `printers`
--
ALTER TABLE `printers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `printers`
--
ALTER TABLE `printers`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
