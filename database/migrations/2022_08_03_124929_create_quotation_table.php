<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation', function (Blueprint $table) {
            $table->id();
            $table->string('page_xs')->nullable();
            $table->string('page_s')->nullable();
            $table->string('page_m')->nullable();
            $table->string('page_l')->nullable();
            $table->string('page_xl')->nullable();
            $table->string('optimization')->nullable();
            $table->string('responsive')->nullable();
            $table->string('framework')->nullable();
            $table->longText('layout')->nullable();
            $table->longText('compatibility')->nullable();
            $table->string('interactive_options')->nullable();
            $table->longText('advanced_js')->nullable();
            $table->longText('additional_options')->nullable();
            $table->string('page_total')->nullable();
            $table->string('options_total')->nullable();
            $table->string('total')->nullable();
            $table->string('eta')->nullable();
            $table->longText('extraParam')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation');
    }
}
