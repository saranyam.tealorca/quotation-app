<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary', function (Blueprint $table) {
            $table->id();
          
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('mobile')->nullable();
            $table->longText('pagesize')->nullable();
            $table->longText('optimize')->nullable();
            $table->longText('responseive')->nullable();
            $table->longText('framework')->nullable();
            $table->longText('layout')->nullable();
            $table->longText('additional_css_option')->nullable();
            $table->longText('advanced_js_option')->nullable();
            $table->longText('interactivity_option')->nullable();
            $table->longText('compatibility_option')->nullable();
            $table->string('totalcost')->nullable();
            $table->longText('projectbrief')->nullable();
            $table->longText('attachment')->nullable();
            $table->longText('extraParam')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary');
    }
}