<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quotation;

class QuotationController extends Controller
{
    //get api
    public function getApi(Request $request,$id){

     //dd($id);
        /* -------------------------------- TODO: Logic -------------------------------- */
        $responce['success'] = false;
        $responce['message'] = 'No data available';
        if($id){
        $quote=Quotation::find($id);
        $responce['data'] = $quote;
        }else{
        $quote=Quotation::all();
        // foreach($quote as $quotes){
        // $quote->pagesize=json_encode($quotes->pagesize);
        // $quote->optimize=json_encode($quotes->optimize);
        // $quote->responseive=json_encode($quotes->responseive);
        // $quote->framework=json_encode($quotes->framework);
        // $quote->layout=json_encode($quotes->layout);
        // $quote->additional_css_option=json_encode($quotes->additional_css_option);
        // $quote->advanced_js_option=json_encode($quotes->advanced_js_option);
        // $quote->interactivity_option=json_encode($quotes->interactivity_option);
        // $quote->compatibility_option=json_encode($quotes->compatibility_option);
        // $quote->projectbrief=json_encode($quotes->projectbrief);
        $responce['data'] = $quote;
        }
        $responce['success'] = true;
        $responce['message'] = 'Successfully created';
    //}
    //dd($responce);
        return response()->json($responce,200);
    }


    //postapi eod status code
    public function postApi(Request $request){

        $responce['success'] = false;
        $responce['message'] = 'unable to save';
        $quote=new Quotation();
        $quote->pagesize=json_encode($request->pagesize);
        $quote->optimize=json_encode($request->optimize);
        $quote->responseive=json_encode($request->responseive);
        $quote->framework=json_encode($request->framework);
        $quote->layout=json_encode($request->layout);
        $quote->additional_css_option=json_encode($request->additional_css_option);
        $quote->advanced_js_option=json_encode($request->advanced_js_option);
        $quote->interactivity_option=json_encode($request->interactivity_option);
        $quote->compatibility_option=json_encode($request->compatibility_option);
        $quote->projectbrief=json_encode($request->projectbrief);
        $quote->extraParam=json_encode($request->all()); 
        $quote->attachment=$request->attachment;
        $quote->totalcost=$request->totalcost;
        $contact = $request->contact;
        $quote->name=$contact['name'];
        $quote->email=$contact['email'];
        $quote->mobile=$contact['mobile'];
        //dd($contact['name']);
        $quote->save();
       
       
       
            $responce['data'] = $quote;
            $responce['success'] = true;
            $responce['message'] = 'Successfully created';
        // }

        
        
        return response()->json($responce,200);
      
    }

    public function list(){
      $quote=Quotation::paginate(20);
      return view('show',compact('quote'));
    }

}