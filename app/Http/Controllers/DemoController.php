<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quotation;
class DemoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mytime =Carbon::now();
      
      $date = Carbon::parse( $mytime->toDateString());
      $date=date_format($date,"d/m/Y");
    //   echo $date;
    //   dd($date);
        return view('print',compact('date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $quote=Quotation::all();
        // foreach($quote as $quotes){
        //  $quot=json_decode($quotes->pagesize);
        //  $users=[];
         //$users[]=$quot;
         //$keys = array_keys($quot);
         //    echo "<pre>";
             //print_r($quot);
        //  foreach($quot as $key => $value){
        //   //   echo $value->option;
        //      $users[]=$value;
        // echo "<pre>";
        // print_r($quot);
        //  }
          
  //dd($quote);
        
       
        return view('demo',compact('quote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function edit(Printer $printer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Printer $printer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Printer $printer)
    {
        //
    }
}
