<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print page</title>
    <style>
        span {
            float: right;
            color: rgb(30, 210, 150);
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <div class="row  justify-content-between ">
            <div class="col-md-10 col-sm-12">
                <div class="display-center"><img src="logo/hk-logo.png" alt=""></div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-body">
                                                <div style=" color:rgb(30, 51, 210)" class="mt-5 p-4">XL</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 ml-3">
                                            <div class="card">
                                                <div class="card-header">
                                                    Example</div><br>
                                                <div class="card-body">
                                                    <p>About Us (text page only), Terms of Use, Privacy Policy, Single
                                                        Blog
                                                        Post, 404 page, Other text pages.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card ml-3">
                                            <div class="card-body">
                                                <div style=" color:rgb(30, 210, 150)" class="mt-5 p-4">$99</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Optimization and Accessibility</h4>
                                    </div>
                                    <p>We create clean and optimized code that meets the rigorous W3C standards. For its
                                        full
                                        compliance with the accessibility requirements, select this option:</p>
                                    <p>Section 508 / WCAG <span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Responsiveness</h4>
                                    </div>
                                    <p>I have a design <span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Framework</h4>
                                    </div>
                                    <div style="">
                                        <p>Bootstrap <span>+15</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Layout Look</h4>
                                    </div>
                                    <p>Retina <span>+15</span>
                                    </p>
                                    <p>Fonts <span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Compatibility</h4>
                                        <div style="opacity: 0.5">
                                            <p>Compatible with all modern browsers and devices.</p>
                                        </div>
                                    </div>
                                    <p>Compatible with and tested in/on Google Chrome, Mozilla Firefox, Safari 14+,
                                        Opera,
                                    </p>
                                    <p>Edge, IOS 14+ (IPhone, IPad), Android 9+ <span>+15</span>
                                    </p>
                                    <p>Another/older browser<span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Interactivity options
                                        </h4>
                                    </div>
                                    <h3>JS/CSS interactivity options
                                    </h3><br>
                                    <p>Standard interactivity pack <span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Advanced JS functionality</h4>
                                    </div>
                                    <p> <b>Note: </b>our JS development rates are <span style="color:aquamarine">$35-$45
                                        </span>per hour. We will provide the final price upon a thorough analysis of
                                        your designs and requirements.</p>
                                    <p>Angular <span>+15</span> </p>
                                    <p>D3 <span>+15</span> </p>
                                    <p>jQuery <span>+15</span> </p>
                                    <p>Native JS <span>+15</span> </p>
                                    <p>jQuery UI <span>+15</span> </p>
                                    <p>If you need anything beyond this list, let us know in the Project brief below.
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Additional CSS/JS Options</h4>
                                    </div>
                                    <p> <b>Note: </b>our JS development rates are <span id="var"
                                            style="color:aquamarine">$35-$45
                                        </span>per hour. We will provide the final price upon a thorough analysis of
                                        your designs and requirements.</p>
                                    <p>Advanced CSS3 Animation <span>+15</span>
                                    </p>
                                    <p>Advanced Parallax <span>+15</span> </p>
                                    <p>Google Maps <span>+15</span>
                                    </p>
                                </div>
                            </div>
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Framework</h4>
                                    </div>
                                    <div style="">
                                        <p>Bootstrap <span>+15</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div style="margin-left:76%;"><button type="button" class="btn btn-primary" onclick="printwindow(this)"> print
            </button></div>

    </div>

</body>

</html>
<script>
    function printwindow(val) {
        // alert(val)
        window.print();
    }
</script>
