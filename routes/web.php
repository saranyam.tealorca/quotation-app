<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\PrinterController;
use App\Http\Controllers\DemoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//API routes
// Route::get('getapi', function(){
//     dd(Uuid::generate()->string);},[QuotationController::class, 'getApi']);
Route::get('/print/{id}',[PrinterController::class,'index'])->name('print');
Route::get('list', [QuotationController::class, 'list']);
//uuid routes
// Route::get('uuid', function () {
//     dd(Uuid::generate()->string);
// });
