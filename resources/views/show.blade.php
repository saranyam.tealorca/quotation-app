<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div>
                <h1>Quotation List Page</h1>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 pb-5">
                        <table class="table table-bordered table-hover mt-3 table-striped border" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Total cost</th>
                                    <th scope="col" class="custom-no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($quote as $key => $new)
                                    <tr class="border">
                                        <th scope="row" class="text-center">
                                            {{ ($quote->currentpage() - 1) * $quote->perpage() + $key + 1 }}
                                        </th>
                                        <td>{{ $new->name }}</td>
                                        <td>{{ $new->email }}</td>
                                        <td>{{ $new->mobile }}</td>
                                        <td>{{ $new->totalcost }}</td>
                                        <td class="custom-no-sort">
                                            <a href="{{ route('print', ['id' => $new->id]) }}" class="btn btn-primary">
                                                view
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-12">
                                {{ $quote->appends(Request::all())->links('pagination::bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
