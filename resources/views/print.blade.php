<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print page</title>
    <style>
        span {
            float: right;
            color: rgb(30, 210, 150);
        }

        .dev {
            float: right;
        }

        .row.attach {
            margin-left: 29px;
            margin-top: 2%;
        }

        .card.attach-add {
            width: 97%;
            margin-left: 13px;
            margin-bottom: 2%;
        }

        .blue-fade {
            text-align: center;
            font-weight: bold;
            font-size: 55px;
            font-family: Helvetica, Arial, sans-serif;
            /* font-weight: 100; */
            background: -webkit-linear-gradient(#ef0000, #3f0000);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    {{-- @foreach ($quote as $qt) --}}
    <div class="container">
        <div class="row  justify-content-between ">
            <div class="col-md-10 col-sm-12">
                <div class="display-center"><img src="{{ asset('logo/hk-logo.png') }}" alt="logo">
                    <p class="dev">Date :{{ $date }} </p>
                </div>
                <div class="card-body">
                    <div class="card attach-add p-2">
                        <div>
                            <h3>Selected page sizes</h3>
                        </div>
                        @if ($qt->pagesize)
                            @php
                                $quot = json_decode($qt->pagesize);
                            @endphp
                            @foreach ($quot as $key => $value)
                                <div class="form-group row">
                                    <div class="row attach">
                                        <div class="card">
                                            <div class="card-body">
                                                <div style=" color:rgb(30, 51, 210)" class="mt-5 p-4"><b>
                                                        {{ $value->option }}</b></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="card">
                                                <div class="card-header">
                                                    Example</div><br>
                                                <div class="card-body">
                                                    <p>About Us (text page only), Terms of Use, Privacy Policy,
                                                        Single
                                                        Blog
                                                        Post, 404 page, Other text pages.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div style=" color:rgb(30, 210, 150)" class="mt-5 p-4">
                                                    <b>${{ $value->price }}</b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-sm-12">
                        @if ($qt->optimize)
                            @php
                                $json = json_decode($qt->optimize);
                            @endphp

                            <div class="card">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Optimization and Accessibility</h4>
                                    </div>
                                    <p>We create clean and optimized code that meets the rigorous W3C standards. For
                                        its
                                        full
                                        compliance with the accessibility requirements, select this option:</p>
                                    @foreach ($json as $key => $value)
                                        <p>{{ $value->option }} <span> {{ $value->pc }}%</span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if ($qt->responseive)
                            @php
                                $anil = json_decode($qt->responseive);
                            @endphp
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Responsiveness</h4>
                                    </div>
                                    <p>{{ $anil->option }}<span>{{ $anil->pc }}%</span>
                                    </p>
                                </div>
                            </div>
                        @endif
                        @if ($qt->framework)
                            @php
                                $var = json_decode($qt->framework);
                            @endphp
                            <div class="card mt-1">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Framework</h4>
                                    </div>
                                    <div style="">
                                        <p>{{ $var->option }} <span>{{ $var->pc }}%</span></p>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if ($qt->layout)
                            @php
                                $quot = json_decode($qt->layout);
                            @endphp

                            <div class="card mt-2">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Layout Look</h4>
                                    </div>
                                    @foreach ($quot as $key => $value)
                                        <p> {{ $value->option }} <span> {{ $value->pc }}%</span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if ($qt->compatibility_option)
                            @php
                                $fil = json_decode($qt->compatibility_option);
                            @endphp
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Compatibility</h4>
                                        <div style="opacity: 0.5">
                                            <p>Compatible with all modern browsers and devices.</p>
                                        </div>
                                    </div>
                                    <p>Compatible with and tested in/on Google Chrome, Mozilla Firefox, Safari 14+,
                                        Opera,
                                    </p>
                                    @foreach ($fil as $key => $value)
                                        <p>{{ $value->option }}<span>{{ $value->pc }}%</span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if ($qt->interactivity_option)
                            @php
                                $far = json_decode($qt->interactivity_option);
                            @endphp
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Interactivity options</h4>
                                    </div>
                                    <h3>JS/CSS interactivity options
                                    </h3><br>
                                    @foreach ($far as $key => $value)
                                        <p>{{ $value->option }}<span>${{ $value->cost }}</span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if ($qt->advanced_js_option)
                            @php
                                $van = json_decode($qt->advanced_js_option);
                            @endphp
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Advanced JS functionality</h4>
                                    </div>
                                    <p> <b>Note: </b>our JS development rates are <span
                                            style="color:rgb(30, 210, 150)">$35-$45
                                        </span>per hour. We will provide the final price upon a thorough analysis of
                                        your designs and requirements.</p>
                                    @foreach ($van as $key => $value)
                                        <p>{{ $value->option }}<span>${{ $value->costPerHr }}</span> </p>
                                    @endforeach
                                    <p>If you need anything beyond this list, let us know in the Project brief
                                        below.
                                    </p>
                                </div>
                            </div>
                        @endif
                        @if ($qt->additional_css_option)
                            @php
                                $val = json_decode($qt->additional_css_option);
                            @endphp
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class=title>
                                        <h4>Additional CSS/JS Options</h4>
                                    </div>
                                    <p> <b>Note: </b>our JS development rates are <span id="var"
                                            style="color:rgb(30, 210, 150)">$35-$45
                                        </span>per hour. We will provide the final price upon a thorough analysis of
                                        your designs and requirements.</p>
                                    </p>
                                    @foreach ($val as $key => $value)
                                        <p>{{ $value->option }}<span>${{ $value->costPerHr }}</span> </p>
                                    @endforeach
                                    </p>
                                </div>
                            </div>
                        @endif
                        <div class="card mt-1">
                            <div class="card-body">
                                <div class=title>
                                    <h4>Total</h4>
                                </div>
                                <br>
                                <p>Total cost :<span>${{ $qt->totalcost }}</span></p>
                            </div>
                        </div>
                        <div class="card mt-1">
                            <div class="card-body">
                                <div class=title>
                                    <h4>Contact details</h4>
                                </div>
                                <br>
                                <p>Name :{{ $qt->name }}</p>
                                <p>Email :{{ $qt->email }}</p>
                                <p>M0bile :{{ $qt->mobile }}</p>
                            </div>
                        </div>
                        <div class="card mt-1">
                            <div class="card-body">
                                <div class=title>
                                    <h4>Attachments</h4>
                                </div>
                                <br>
                                <p>Attachments :{{ $qt->attachment }}</p>
                            </div>
                        </div>
                        <div class="card mt-1">
                            <div class="card-body">
                                <div class=title>
                                    <h4>Project brief</h4>
                                </div>
                                <div class="card">
                                    <p>{{ $qt->projectbrief }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <br>
                            <h1 class="blue-fade">Thank you</h1>
                        </div>
                    </div>
                    <div class="dev mt-2"><button type="button" class="btn btn-primary" onclick="printwindow(this)">
                            print
                        </button></div>
                </div>
            </div>
        </div>
    </div>
    {{-- @endforeach --}}
</body>

</html>
<script>
    function printwindow(val) {
        // alert(val)
        window.print();
    }
</script>
